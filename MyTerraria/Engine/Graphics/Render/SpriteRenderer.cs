﻿using MyTerraria.Engine.Helpers;

namespace MyTerraria.Engine.Graphics;

public static class SpriteRenderer
{
    private static BufferObject<float> _vbo;
    private static BufferObject<uint> _ebo;
    private static VertexArrayObject<float, uint> _vao;
    
    private static uint _drawCount;

    private static Shader _shader;
    private static Texture _texture;

    public static Camera Camera = new();
    
    private static List<float> _vertices = new();
    private static List<uint> _indices = new();
    public static void Init()
    {
        _shader = Game.Resources.Shaders["SpriteShader"];
        _texture = Game.Resources.Textures["whitePixel"];
        _vbo = new BufferObject<float>(BufferTargetARB.ArrayBuffer);
        _ebo = new BufferObject<uint>(BufferTargetARB.ElementArrayBuffer);
        _vao = new VertexArrayObject<float, uint>(Game.Gl, _vbo, _ebo);
        _vao.VertexAttributePointer(0, 2, VertexAttribPointerType.Float, 4, 0);
        _vao.VertexAttributePointer(1, 2, VertexAttribPointerType.Float, 4, 2);
    }

    public static void Draw(in Vertex2DUv[] vertices, in uint[] indices, Texture texture)
    {
        // TODO: тратить только 1 drawcall на одну текстуру
        // TODO: генерировать атлас текстур и рисовать 1 дроу коллом
        if (_texture.Handle != texture.Handle)
        {
            Flush();
            _texture = texture;
        }
        foreach (var vertex in vertices)
        {
            _vertices.Add(vertex.Position.X);
            _vertices.Add(vertex.Position.Y);
            _vertices.Add(vertex.Uv.X);
            _vertices.Add(vertex.Uv.Y);
        }
        foreach (var index in indices)
        {
            _indices.Add(index + _drawCount * 4);
        }
        _drawCount++;
    }

    public static unsafe void Flush()
    {
        if (_vertices.Count == 0)
            return;
        _vao.Bind();
        _ebo.BufferData(_indices.ToSpan());
        _vbo.BufferData(_vertices.ToSpan());

         _shader.Use();
        _texture.Bind();

        _shader.SetUniform("uTexture0", 0);
        _shader.SetUniform("uCamera", Camera.ViewMatrix);
        _shader.SetUniform("uProjectionMatrix", Game.ProjectionMatrix);

        Game.Gl.DrawElements(PrimitiveType.Triangles, (uint) _indices.Count, DrawElementsType.UnsignedInt, null);
        _vertices.Clear();
        _indices.Clear();
        _drawCount = 0;
    }
}