﻿namespace MyTerraria.Engine.Graphics;

public class RectangleRenderer : IRenderer
{
    private static BufferObject<float> _vbo;
    private static BufferObject<uint> _ebo;
    private static VertexArrayObject<float, uint> _vao;
    
    private static uint _drawCount;

    private static Shader _shader;

    private static List<float> _vertices = new();
    private static List<uint> _indices = new();
    public static void Init()
    {
        _shader = Game.Resources.Shaders["PrimitiveShader"];
        _vbo = new BufferObject<float>(BufferTargetARB.ArrayBuffer);
        _ebo = new BufferObject<uint>(BufferTargetARB.ElementArrayBuffer);
        _vao = new VertexArrayObject<float, uint>(Game.Gl, _vbo, _ebo);
        _vao.VertexAttributePointer(0, 2, VertexAttribPointerType.Float, 5, 0);
        _vao.VertexAttributePointer(1, 3, VertexAttribPointerType.Float, 5, 2);
    }

    public static void Draw(in Vertex2DRgb[] vertices, in uint[] indices)
    {
        foreach (var vertex in vertices)
        {
            _vertices.Add(vertex.Position.X);
            _vertices.Add(vertex.Position.Y);
            _vertices.Add(vertex.Color.X);
            _vertices.Add(vertex.Color.Y);
            _vertices.Add(vertex.Color.Z);
        }
        foreach (var index in indices)
        {
            _indices.Add(index + _drawCount * 4);
        }
        _drawCount++;
    }

    public static unsafe void Flush()
    {
        if (_vertices.Count == 0)
            return;
        _vao.Bind();
        _ebo.BufferData(_indices.ToSpan());
        _vbo.BufferData(_vertices.ToSpan());

         _shader.Use();
         // TODO: камера должна быть не в спрайтрендерере
         _shader.SetUniform("uCamera", SpriteRenderer.Camera.ViewMatrix);
        _shader.SetUniform("uProjectionMatrix", Game.ProjectionMatrix);

        Game.Gl.DrawElements(PrimitiveType.Triangles, (uint) _indices.Count, DrawElementsType.UnsignedInt, null);
        _vertices.Clear();
        _indices.Clear();
        _drawCount = 0;
    }
}