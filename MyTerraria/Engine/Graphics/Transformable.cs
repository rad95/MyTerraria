﻿namespace MyTerraria.Engine.Graphics;

public abstract class Transformable
{
    protected Vector2 _origin;
    private Vector2 _position;
    private float _rotation;
    private Vector3 _scale;

    // TODO: Почему то здесь текстура

    public virtual Vector2 Size { get; set; }

    public Vector2 Center => Size / 2;
    public Vector2 BottomCenter => new(Size.X / 2, 0);

    public virtual Vector2 Origin
    {
        get => _origin;
        set
        {
            _origin = value;
            Transform.Origin = new Vector3((value), 0);
            OnTransformChanged();
        }
    }

    public virtual Vector2 Position
    {
        get => _position;
        set
        {
            _position = value;
            Transform.Position = new Vector3(value, 0);
            OnTransformChanged();
        }
    }

    public void Move(float x, float y)
    {
        Position += new Vector2(x, y);
    }
    public void MoveX(float x)
    {
        Position += new Vector2(x, 0);
    }

    public void MoveY(float y)
    {
        Position += new Vector2(0, y);
    }


    public virtual float Rotation
    {
        get => _rotation;
        set
        {
            _rotation = value;
            Transform.Rotation = Quaternion.CreateFromAxisAngle(Vector3.UnitZ, value);
            OnTransformChanged();
        }
    }

    public virtual Vector3 Scale
    {
        get => _scale;
        set
        {
            _scale = value;
            Transform.Scale = value;
            OnTransformChanged();
        }
    }

    public virtual void OnTransformChanged(){}
    public Transform Transform { get; set; } = new();
}