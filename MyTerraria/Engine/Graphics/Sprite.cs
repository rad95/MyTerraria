﻿using System.Runtime.CompilerServices;

namespace MyTerraria.Engine.Graphics;

public class Sprite : DrawableObject
{
    public Texture Texture { get; set; }
    private static readonly uint[] indices =
    {
        0, 1, 2,
        0, 2, 3
    };
    private static readonly uint[] outlineIndices =
    {
        0, 1, 2, 3, 
        1, 2, 0, 3
    };

    private bool _shouldUpdateVertices = true;

    private Vector2 _size;

    private readonly Vertex2DUv[] _vertices =
    {
        new(0, 0, 0, 0),
        new(0, 0, 0, 1),
        new(0, 0, 1, 1),
        new(0, 0, 1, 0)
    };
    
    private Vertex2DUv[] _transformedVertices =
    {
        new(0, 0, 0, 0),
        new(0, 0, 0, 1),
        new(0, 0, 1, 1),
        new(0, 0, 1, 0)
    };

    public override Vector2 Origin
    {
        get
        {
            return base.Origin;
        }
        set
        {
            base.Origin = value;
            // left bottom
            _vertices[0].Position = new Vector2(0 - value.X, 0 - value.Y); 
            // left top
            _vertices[1].Position = new Vector2(0 - value.X, Size.Y - value.Y);
            //right top
            _vertices[2].Position = new Vector2(Size.X - value.X, Size.Y- value.Y);
            //right bottom
            _vertices[3].Position = new Vector2(Size.X - value.X, 0- value.Y);
        }
    }

    private Vertex2DRgb[] _outlineVertices = new Vertex2DRgb[4];


    public Sprite(Vector2 size, Vector2 pos) : base()
    {
        // вычислять по оригину

        Size = size;
        Position = pos;
        //Origin = size / 2;
        Rotation = 0;
        Scale = new(1);
    }
    public Sprite(Texture texture, Vector2 size) : base()
    {
        Texture = texture;
        Size = size;
        Origin = size / 2;
        Rotation = 0;
        Scale = new(1);
    }

    public Sprite(Texture texture, Vector2 size, Vector2 pos) : this(texture, size)
    {
        Position = pos;
    }

    public override Vector2 Size
    {
        get => _size;
        set
        {
            _size = value;
            // left bottom
            _vertices[0].Position = new Vector2(0 - Origin.X, 0 - Origin.Y); 
            // left top
            _vertices[1].Position = new Vector2(0 - Origin.X, value.Y - Origin.Y);
            //right top
            _vertices[2].Position = new Vector2(value.X - Origin.X, value.Y- Origin.Y);
            //right bottom
            _vertices[3].Position = new Vector2(value.X - Origin.X, 0- Origin.Y);
        }
    }

    public override void OnTransformChanged()
    {
        _shouldUpdateVertices = true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public override void Draw()
    {
        if (_shouldUpdateVertices)
            UpdateTransformedVertices();
        SpriteRenderer.Draw(_transformedVertices, indices, Texture);


    }

    // TODO: create class for color
    public void DrawBorder(Vector3 color)
    {
        if (_shouldUpdateVertices)
            UpdateTransformedVertices();
        _outlineVertices[0].Position = _transformedVertices[0].Position;
        _outlineVertices[0].Color = color;
        _outlineVertices[1].Position = _transformedVertices[1].Position;
        _outlineVertices[1].Color = color;
        _outlineVertices[2].Position = _transformedVertices[2].Position;
        _outlineVertices[2].Color = color;
        _outlineVertices[3].Position = _transformedVertices[3].Position;
        _outlineVertices[3].Color = color;
        LinesRenderer.Draw(_outlineVertices, outlineIndices);
    }

    private void UpdateTransformedVertices()
    {
        //apply transform
        _transformedVertices[0].Position = Transform.ViewMatrix.MultiplyBy(_vertices[0].Position);
        _transformedVertices[1].Position = Transform.ViewMatrix.MultiplyBy(_vertices[1].Position);
        _transformedVertices[2].Position = Transform.ViewMatrix.MultiplyBy(_vertices[2].Position);
        _transformedVertices[3].Position = Transform.ViewMatrix.MultiplyBy(_vertices[3].Position);
        _shouldUpdateVertices = false;
    }

    public bool Intersects(Sprite sprite)
    {
        
        Vector2 l1 = Position - Origin;
        Vector2 r1 = l1 + Size;
        Vector2 l2 = sprite.Position - sprite.Origin;
        Vector2 r2 = l2 + sprite.Size;
        // TODO: убрать отрицание
        return !(l1.X >= r2.X || l2.X >= r1.X || r1.Y <= l2.Y || r2.Y <= l1.Y);


    }
}