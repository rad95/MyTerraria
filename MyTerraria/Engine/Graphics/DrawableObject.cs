﻿namespace MyTerraria.Engine.Graphics;

// TODO: сделать DrawableObject и DrawableObjectWithTexture(метод draw должен реализовываться у обоих)
public abstract class DrawableObject : Transformable
{
    public abstract void Draw();
}