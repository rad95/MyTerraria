﻿namespace MyTerraria.Engine.Graphics;

public class TexturePack
{
    private Texture[,] _textures;
    public TexturePack(string path, int offset, int pieceOfTextureWidth, int pieceOfTextureHeight)
    {
        var texImage = (Image<Rgba32>) Image.Load(path);
        texImage.Mutate(x => x.Flip(FlipMode.Vertical));
        // Количество кусочков по горизонтали и по вертикали
        int xCount = texImage.Width/(pieceOfTextureWidth + offset);
        int yCount = texImage.Height/(pieceOfTextureHeight + offset);
        _textures = new Texture[xCount, yCount];
        
        for (int x = 0; x < xCount; x++)
        {
            for (int y = 0; y < yCount; y++)
            {
                var img = texImage.CropRect(new(x * pieceOfTextureWidth + x + 1, y * pieceOfTextureHeight + y + 1), 
                    new(x * pieceOfTextureWidth + x + pieceOfTextureWidth + 1, y * pieceOfTextureHeight + y + pieceOfTextureHeight + 1));
                img.SaveAsPng($"img{x}{y}");
                _textures[x, y] = new Texture(img);
            }
        }
    }

    public Texture this[int x, int y] => _textures[x, y];
}