﻿namespace MyTerraria.Engine.Graphics;

public struct Vertex2DUv
{
    public const int NumberOfValues = 4;
    public Vertex2DUv(float posX, float posY, float u, float v)
    {
        Position = new Vector2(posX, posY);
        Uv = new Vector2(u, v);
    }

    public Vector2 Position;
    public Vector2 Uv;
}