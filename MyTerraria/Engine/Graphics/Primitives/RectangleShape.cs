﻿namespace MyTerraria.Engine.Graphics.Primitives;

public class RectangleShape : DrawableObject
{
    private static readonly uint[] indices =
    {
        0, 1, 2,
        0, 2, 3
    };
    private static readonly uint[] outlineIndices =
    {
        0, 1, 2, 3, 
        1, 2, 0, 3
    };

    private bool _shouldUpdateVertices = true;

    private Vector2 _size;
    private Vertex2DRgb[] _transformedVertices = new Vertex2DRgb[4]
    {
        new(0, 0, 0, 0, 0),
        new(0, 0, 0, 0, 0),
        new(0, 0, 0, 0, 0),
        new(0, 0, 0, 0, 0)
    };
    private Vertex2DRgb[] _outlineVertices = new Vertex2DRgb[4];
    private Vector3 _color;

    public Vector3 Color
    {
        get => _color;
        set
        {
            _color = value;
            UpdateColor(value);
        }
    }

    private readonly Vertex2DRgb[] _vertices =
    {
        new(0, 0, 0, 0, 0),
        new(0, 0, 0, 0, 0),
        new(0, 0, 0, 0, 0),
        new(0, 0, 0, 0, 0)
    };

    public override Vector2 Origin
    {
        get => base.Origin;
        set
        {
            base.Origin = value;
            // left bottom
            _vertices[0].Position = new Vector2(0 - value.X, 0 - value.Y); 
            // left top
            _vertices[1].Position = new Vector2(0 - value.X, Size.Y - value.Y);
            //right top
            _vertices[2].Position = new Vector2(Size.X - value.X, Size.Y- value.Y);
            //right bottom
            _vertices[3].Position = new Vector2(Size.X - value.X, 0- value.Y);
        }
    }
    


    public RectangleShape(Vector2 size, Vector3 color)
    {
        Color = color;
        Size = size;
        Origin = size / 2;
        Rotation = 0;
        Scale = new(1);
    }

    public RectangleShape(Vector2 size, Vector2 pos, Vector3 color) : this(size, color)
    {
        Position = pos;
    }

    public override Vector2 Size
    {
        get => _size;
        set
        {
            _size = value;
            // left bottom
            _vertices[0].Position = new Vector2(0 - Origin.X, 0 - Origin.Y); 
            // left top
            _vertices[1].Position = new Vector2(0 - Origin.X, value.Y - Origin.Y);
            //right top
            _vertices[2].Position = new Vector2(value.X - Origin.X, value.Y- Origin.Y);
            //right bottom
            _vertices[3].Position = new Vector2(value.X - Origin.X, 0- Origin.Y);
        }
    }

    public override void OnTransformChanged()
    {
        _shouldUpdateVertices = true;
    }
    
    public override void Draw()
    {
        if (_shouldUpdateVertices)
            UpdateTransformedVertices();
        RectangleRenderer.Draw(_transformedVertices, indices);


    }

    // TODO: create class for color
    public void DrawBorder(Vector3 color)
    {
        if (_shouldUpdateVertices)
            UpdateTransformedVertices();
        _outlineVertices[0].Position = _transformedVertices[0].Position;
        _outlineVertices[0].Color = color;
        _outlineVertices[1].Position = _transformedVertices[1].Position;
        _outlineVertices[1].Color = color;
        _outlineVertices[2].Position = _transformedVertices[2].Position;
        _outlineVertices[2].Color = color;
        _outlineVertices[3].Position = _transformedVertices[3].Position;
        _outlineVertices[3].Color = color;
        LinesRenderer.Draw(_outlineVertices, outlineIndices);
    }

    private void UpdateTransformedVertices()
    {
        //apply transform
        _transformedVertices[0].Position = Transform.ViewMatrix.MultiplyBy(_vertices[0].Position);
        _transformedVertices[1].Position = Transform.ViewMatrix.MultiplyBy(_vertices[1].Position);
        _transformedVertices[2].Position = Transform.ViewMatrix.MultiplyBy(_vertices[2].Position);
        _transformedVertices[3].Position = Transform.ViewMatrix.MultiplyBy(_vertices[3].Position);
        _shouldUpdateVertices = false;
    }

    private void UpdateColor(Vector3 color)
    {
        _vertices[0].Color = color;
        _transformedVertices[0].Color = color;
        _vertices[1].Color = color;
        _transformedVertices[1].Color = color;
        _vertices[2].Color = color;
        _transformedVertices[2].Color = color;
        _vertices[3].Color = color;
        _transformedVertices[3].Color = color;
    }

    private void UpdateVertices(Vector2 size)
    {
        
    }
}