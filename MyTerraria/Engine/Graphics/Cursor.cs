﻿namespace MyTerraria.Engine.Graphics;

// TODO: Cursor should be ui element
public class Cursor : Sprite
{
    private Texture _texture;
    public Cursor(Texture texture) : base(texture, texture.Size)
    {
        Origin = new(0, texture.Size.Y);
        _texture = texture;
    }

    public void Update()
    {
        Scale = new Vector3(1, 1, 1) / SpriteRenderer.Camera.Zoom;
        Position = Game.GetMousePos() + (SpriteRenderer.Camera.Position - SpriteRenderer.Camera.Origin);
    }
}