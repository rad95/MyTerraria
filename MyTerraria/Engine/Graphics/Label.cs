﻿using SilkyNvg.Paths;

namespace MyTerraria.Engine.Graphics;

public class Label : Transformable
{
    public Colour Color;
    public string FontName;
    public int FontSize;

    public bool ShouldDrawBackground = true;

    // TODO: реализовать Nvg. методы, например градиент и Rotation(nvg.rotate(value))
    public string Text;

    // TODO: Облегчить инициализацию путем добавления именнованных шрифтов(new(..., font: "Arial"), добавив перед этим его в систему fontManager.Add("Arial", arialPath)
    public Label(string text, Vector2 pos, int fontSize, Colour color, string fontName, string fontPath)
    {
        Text = text;
        Position = pos;
        FontSize = fontSize;
        Color = color;
        FontName = fontName;
        Game.Ng.CreateFont(fontName, fontPath);
    }

    public void Draw()
    {
        Game.Ng.FontSize(FontSize);
        Game.Ng.FillColour(Color);
        Game.Ng.StrokeColour(Colour.Black);
        Game.Ng.Text(Position.X, Position.Y, Text);
        if (ShouldDrawBackground)
        {
            Game.Ng.BeginPath();
            Rectangle<float> rect;
            Game.Ng.TextBounds(Position.X, Position.Y, Text, out rect);
            Game.Ng.RoundedRect(Position.X, Position.Y - rect.Size.Y, rect.Size.X, rect.Size.Y, 3);
            Game.Ng.FillColour(new Colour(136, 0, 255, 125));
            Game.Ng.Fill();
        }
    }

    public void Draw(string text)
    {
        Text = text;
        Game.Ng.FontSize(FontSize);
        Game.Ng.FillColour(Color);
        Game.Ng.Text(Position.X, Position.Y, text);
        if (ShouldDrawBackground)
        {
            Game.Ng.BeginPath();
            Rectangle<float> rect;
            Game.Ng.TextBounds(Position.X, Position.Y, text, out rect);
            Game.Ng.RoundedRect(Position.X, Position.Y - rect.Size.Y, rect.Size.X, rect.Size.Y, 3);
            Game.Ng.FillColour(new Colour(136, 0, 255, 125));
            Game.Ng.Fill();
        }
    }
}