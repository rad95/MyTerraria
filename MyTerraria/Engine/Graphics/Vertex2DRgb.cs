﻿namespace MyTerraria.Engine.Graphics;

public struct Vertex2DRgb
{
    public Vertex2DRgb(float posX, float posY, float r, float g, float b)
    {
        Position = new Vector2(posX, posY);
        Color = new Vector3(r, g, b);
    }

    public Vector2 Position;
    public Vector3 Color;
}