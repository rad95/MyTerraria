﻿namespace MyTerraria.Engine;

public class Rect
{
    public Vector2 Position;
    public Vector2 Size;
    public Vector2 Origin;
    public Vector2 LeftBottomPoint
    {
        get => Position + Origin;
        private set{}
    }
    public Vector2 RightTopPoint
    {
        get => LeftBottomPoint + Size;
        private set{}
    }
    
    public bool Intersects(Sprite sprite)
    {
        
        Vector2 l1 = Position - Origin;
        Vector2 r1 = l1 + Size;
        Vector2 l2 = sprite.Position - sprite.Origin;
        Vector2 r2 = l2 + sprite.Size;
        // TODO: убрать отрицание
        return !(l1.X >= r2.X || l2.X >= r1.X || r1.Y <= l2.Y || r2.Y <= l1.Y);


    }
    
    public bool Intersects(Rect rect)
    {
        
        Vector2 l1 = Position - Origin;
        Vector2 r1 = l1 + Size;
        Vector2 l2 = rect.Position - rect.Origin;
        Vector2 r2 = l2 + rect.Size;
        // TODO: убрать отрицание
        return !(l1.X >= r2.X || l2.X >= r1.X || r1.Y <= l2.Y || r2.Y <= l1.Y);
    }

    public Rect(Vector2 position, Vector2 size, Vector2 origin)
    {
        Position = position;
        Size = size;
        Origin = origin;
    }
}