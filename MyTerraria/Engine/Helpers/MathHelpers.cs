﻿namespace MyTerraria.Engine;

public static class MathHelpers
{
    public static Vector2 MultiplyBy(this Matrix4x4 matrix, Vector2 vector)
    {
        Vector2 res;
        res.X = matrix.M11 * vector.X + matrix.M12 * vector.Y + matrix.M41;
        res.Y = matrix.M21 * vector.X + matrix.M22 * vector.Y + matrix.M42;
        return res;
    }

    public static float DegreesToRadians(this float degrees)
    {
        return 3.14159265359f / 180 * degrees;
    }

    // TODO: delete
    public static string ToStr(this Matrix4x4 mat)
    {
        return $@"{mat.M11.ToString("N4")} {mat.M12} {mat.M13} {mat.M14}
{mat.M21} {mat.M22.ToString("N4")} {mat.M23} {mat.M24}
{mat.M31} {mat.M32} {mat.M33} {mat.M34}
{mat.M41} {mat.M42} {mat.M43} {mat.M44}";
    }

    public static string ToStr(this Matrix4X4<float> mat)
    {
        return $@"{mat.M11} {mat.M21} {mat.M31} {mat.M41}
{mat.M12} {mat.M22} {mat.M32} {mat.M42}
{mat.M13} {mat.M23} {mat.M33} {mat.M43}
{mat.M14} {mat.M24} {mat.M34} {mat.M44}";
    }

    public static string ToStr2(this Matrix4X4<float> mat)
    {
        return $@"{mat.M11} {mat.M12} {mat.M13} {mat.M14}
{mat.M21} {mat.M22} {mat.M23} {mat.M24}
{mat.M31} {mat.M32} {mat.M33} {mat.M34}
{mat.M41} {mat.M42} {mat.M43} {mat.M44}";
    }
}