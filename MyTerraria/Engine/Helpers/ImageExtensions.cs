﻿namespace MyTerraria.Engine;

public static class ImageExtensions
{
    public static Image<Rgba32> CropRect(this Image<Rgba32> img, Vector2D<int> start, Vector2D<int> end)
    {
        var res = new Image<Rgba32>(Math.Abs(end.X - start.X), Math.Abs(end.X - start.X));
        int i = 0, j = 0;
        for (int x = start.X; x < end.X; x++)
        {
            for (int y = start.Y; y < end.Y; y++)
            {
                res[i, j] = img[x, y];
                i++;
                j++;
            }
        }

        return res;
    }
}