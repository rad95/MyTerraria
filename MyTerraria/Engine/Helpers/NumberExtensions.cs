﻿namespace MyTerraria.Engine.Helpers;

public static class NumberExtensions
{
    public static bool IsWithin(this int value, int minimum, int maximum)
    {
        return value >= minimum && value <= maximum;
    }
    public static bool IsWithin(this float value, int minimum, int maximum)
    {
        return value >= minimum && value <= maximum;
    }
}