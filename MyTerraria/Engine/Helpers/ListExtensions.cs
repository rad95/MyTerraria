﻿namespace MyTerraria.Engine.Helpers;

public static class ListExtensions
{
    public static Span<T> ToSpan<T>(this List<T> list)
    {
        return CollectionsMarshal.AsSpan(list);
    }
}