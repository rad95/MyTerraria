﻿using System.Text;

namespace MyTerraria.Engine.Graphics;

public static class Vertex2DuvExtensions
{
    public static float[] ToFloatArray(this Vertex2DUv[] vertexes)
    {
        List<float> res = new(vertexes.Length * Vertex2DUv.NumberOfValues);
        foreach (var v in vertexes)
        {
            res.Add(v.Position.X);
            res.Add(v.Position.Y);
            res.Add(v.Uv.X);
            res.Add(v.Uv.Y);
        }

        return res.ToArray();
    }

    public static void PrintPositions(this Vertex2DUv[] vertexes)
    {
        Console.WriteLine(vertexes.GetPositionsAsString());
    }

    //float
    public static void PrintArrayAsVertices(this List<float> vertices, int vertexLength)
    {
        Console.WriteLine(GetVerticesAsString(vertices, vertexLength));
    }

    public static string GetVerticesAsString(this List<float> vertices, int vertexLength)
    {
        StringBuilder sb = new();
        var t = 15;
        for (var i = 0; i < vertices.Count; i++)
        {
            if (i % vertexLength == 0 && i != 0)
                sb.AppendLine();
            sb.Append($"{vertices[i]}, ");
            if (i == t)
            {
                t += 16;
                sb.AppendLine();
                sb.AppendLine();
            }
        }

        return sb.ToString();
    }

    //float
    public static void PrintArrayAsVertices(this float[] vertices, int vertexLength)
    {
        for (var i = 0; i < vertices.Length; i++)
        {
            if (i % vertexLength == 0 && i != 0)
                Console.WriteLine();
            Console.Write($"{vertices[i]}, ");
            if (i % 15 == 0 && i != 0)
            {
                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }

    public static void PrintArrayAsIndices(this uint[] indices)
    {
        for (var i = 0; i < indices.Length; i++)
        {
            if (i % 3 == 0 && i != 0)
                Console.WriteLine();
            Console.Write($"{indices[i]}, ");
            if (i % (indices.Length - 1) == 0 && i != 0)
            {
                Console.WriteLine();
                Console.WriteLine("================================================");
            }
        }
    }

    public static void PrintArrayAsIndices(this List<uint> indices)
    {
        Console.WriteLine(GetIndicesAsString(indices));
    }

    public static string GetIndicesAsString(this List<uint> indices)
    {
        StringBuilder sb = new();
        for (var i = 0; i < indices.Count; i++)
        {
            if (i % 3 == 0 && i != 0)
                sb.AppendLine();
            sb.Append($"{indices[i]}, ");
            if (i % (indices.Count - 1) == 0 && i != 0)
            {
                sb.AppendLine();
                sb.AppendLine("================================================");
            }
        }

        return sb.ToString();
    }

    public static string GetPositionsAsString(this Vertex2DUv[] vertexes)
    {
        var sb = new StringBuilder();
        foreach (var v in vertexes) sb.AppendLine($"({v.Position.X}; {v.Position.Y})");

        return sb.ToString();
    }
}