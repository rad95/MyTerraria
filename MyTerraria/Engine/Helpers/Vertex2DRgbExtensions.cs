﻿namespace MyTerraria.Engine.Graphics;

public static class Vertex2DRgbExtensions
{
    public static float[] ToFloatArray(this Vertex2DRgb[] vertexes)
    {
        List<float> res = new();
        foreach (var v in vertexes) res.AddRange(new[] {v.Position.X, v.Position.Y, v.Color.X, v.Color.Y, v.Color.Z});

        return res.ToArray();
    }
}