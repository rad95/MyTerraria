﻿using System.Diagnostics;
using MyTerraria.Engine.Logging;
using Silk.NET.GLFW;
using Cursor = MyTerraria.Engine.Graphics.Cursor;

namespace MyTerraria.Engine;

public class Game
{
    public static IWindow Window;
    public static GL Gl;
    public static Nvg Ng;

    private static IKeyboard primaryKeyboard;
    public static IMouse Mouse;
    public static Resources Resources = new();
    public static float Delta;
    public static float DeltaSum;
    public static Matrix4x4 ProjectionMatrix;
    private static readonly Logger Logger = new(@"C:\Users\jopka\Desktop\logi");

    private bool _useCustomCursor;
    public bool UseCustomCursor
    {
        get => _useCustomCursor;
        set
        {
            _useCustomCursor = value;
            Mouse.Cursor.CursorMode = value ? CursorMode.Hidden : CursorMode.Normal ;
        }
    }

    public Cursor Cursor;


    public void Run()
    {
        var options = WindowOptions.Default with
        {
            Title = "LearnOpengl",
            FramesPerSecond = 0,
            API = new GraphicsAPI(ContextAPI.OpenGL, ContextProfile.Core, ContextFlags.ForwardCompatible, new APIVersion(4, 6)),
            VSync = false,
            WindowState = WindowState.Maximized,
            IsVisible = false
        };
        ProjectionMatrix = Matrix4x4.CreateOrthographicOffCenter(0, options.Size.X, 0, options.Size.Y, 1f, 0f);
        Window = Silk.NET.Windowing.Window.Create(options);

        //Assign events.
        Window.Load += OnLoad;
        Window.Update += OnUpdate;
        Window.Render += OnRenderFrame;
        Window.Resize += OnWindowResize;
        Window.Closing += OnClosing;

        //Run the window.
        Window.Run();
    }

    private void OnWindowResize(Vector2D<int> e)
    {
        Gl.Viewport(0, 0, (uint) Window.Size.X, (uint) Window.Size.Y);
        ProjectionMatrix = Matrix4x4.CreateOrthographicOffCenter(0, e.X, 0, e.Y, 1f, 0f);
    }

    private void OnClosing()
    {
    }

    public virtual unsafe void OnLoad()
    {
        Gl = GL.GetApi(Window);
        
        OnWindowResize(Window.Size);
        Window.IsVisible = true;

        //Set-up input context.
        var input = Window.CreateInput();
        primaryKeyboard = input.Keyboards.FirstOrDefault();
        Mouse = input.Mice.FirstOrDefault();

        if (primaryKeyboard != null) primaryKeyboard.KeyDown += Input.OnKeyDown;
        AddDefaultKeyBindings();
        Gl.ClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        
        OpenGLRenderer nvgRenderer = new(CreateFlags.Antialias | CreateFlags.StencilStrokes | CreateFlags.Debug, Gl);
        Ng = Nvg.Create(nvgRenderer);
    }

    private void AddDefaultKeyBindings()
    {
        Input.AddKeyBinding(Key.Escape, () => Window.Close());
        Input.AddKeyBinding(Key.F11, () =>
            Window.WindowState = Window.WindowState == WindowState.Fullscreen
            ? WindowState.Normal
            : WindowState.Fullscreen);
        Input.AddKeyBinding(Key.F10, () => 
            Process.Start("explorer.exe", $"{Environment.CurrentDirectory}\\screenshots"));
        Input.AddKeyBinding(Key.F12, () =>
        {
            if (!Directory.Exists("screenshots"))
                Directory.CreateDirectory("screenshots");
            TakeScreenshot()
                .Save($@"screenshots\screen{DateTime.Now.ToString().Replace(":", ".").Replace(" ", "_")}.png");
        });
        
    }

    public virtual void OnRender(double delta)
    {
    }

    public virtual void OnRenderFrame(double delta)
    {
        Delta = (float) delta;
        DeltaSum += Delta;
        Gl.Clear((uint) ClearBufferMask.ColorBufferBit);
        var winSize = Window.Size.As<float>();
        var fbSize = Window.FramebufferSize.As<float>();
        var pxRatio = fbSize.X / winSize.X;
        Ng.BeginFrame(Window.Size.As<float>(), pxRatio);
        OnRender(delta);
        if(UseCustomCursor)
            Cursor?.Draw();
           
        LinesRenderer.Flush();
        RectangleRenderer.Flush();
        SpriteRenderer.Flush();
        Ng.EndFrame();
    }

    public virtual void OnUpdate(double obj)
    {
        if(UseCustomCursor)
            Cursor?.Update();
    }

    public static bool IsKeyPressed(Key key)
    {
        return primaryKeyboard.IsKeyPressed(key);
    }

    public static void OnKeyDown(Key key)
    {
    }

    public static void Log(string log)
    {
        Logger.Log(log);
    }

  

    public static Vector2 GetMousePos()
    {
        return new Vector2(Mouse.Position.X, Window.Size.Y - Mouse.Position.Y);
    }

    public unsafe Image<Rgba32> TakeScreenshot()
    {
        var width = (uint) Window.Size.X;
        var height = (uint) Window.Size.Y;
        Image<Rgba32> screenshot = new((int) width, (int) height);
        fixed (void* data = &MemoryMarshal.GetReference(screenshot.GetPixelRowSpan(0)))
        {
            Gl.ReadPixels(0, 0, width, height, PixelFormat.Rgba, PixelType.UnsignedByte, data);
        }

        screenshot.Mutate(c => c.Flip(FlipMode.Vertical));
        return screenshot;
    }
}