﻿namespace MyTerraria.Engine;

//TODO: добавить сочетания клавиш
public static class Input
{
    private static Dictionary<Key, List<Action>> _keyBindings = new();

    public static void AddKeyBinding(Key key, Action action)
    {
        if (!_keyBindings.ContainsKey(key))
            _keyBindings.Add(key, new());
        _keyBindings[key].Add(action);

    }

    public static void OnKeyDown(IKeyboard keyboard, Key key, int code)
    {
        if (_keyBindings.ContainsKey(key))
            foreach (var action in _keyBindings[key])
                action();
    }
}