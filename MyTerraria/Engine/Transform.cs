﻿namespace MyTerraria.Engine;

public class Transform
{
    public Vector3 Position { get; set; } = new(0, 0, 0);

    public Vector3 Scale { get; set; } = new(1);

    public Quaternion Rotation { get; set; } = Quaternion.Identity;

    // TODO: origin должен быть только у CameraTransform(create new class)
    public Vector3 Origin { get; set; } = new(0, 0, 0);


    //
    public Matrix4x4 ViewMatrix => Matrix4x4.Identity *
                                   Matrix4x4.CreateScale(Scale) *
                                   Matrix4x4.CreateFromQuaternion(Rotation) *
                                   Matrix4x4.CreateTranslation(Position);

    public Matrix4x4 CameraViewMatrix => Matrix4x4.Identity *
                                         Matrix4x4.CreateScale(Scale) *
                                         Matrix4x4.CreateTranslation(Position) *
                                         Matrix4x4.CreateFromQuaternion(Rotation) *
                                         Matrix4x4.CreateTranslation(-Position) *
                                         Matrix4x4.CreateTranslation(Position * Scale + Origin);
    
}