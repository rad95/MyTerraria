﻿namespace MyTerraria.Engine;

// TODO: добавить папку в котором хранятся текстуры и шейдеры
// @"Resources/Textures/dirt.png" => @"dirt.png"
public class Resources
{
    public Dictionary<string, Shader> Shaders;

    public Dictionary<string, Texture> Textures;
}