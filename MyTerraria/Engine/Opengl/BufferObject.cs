﻿namespace MyTerraria.Engine.Opengl;

//Our buffer object abstraction.
public class BufferObject<TDataType> : IDisposable
    where TDataType : unmanaged
{
    private readonly BufferTargetARB _bufferType;

    //Our handle, buffertype and the GL instance this class will use, these are private because they have no reason to be public.
    //Most of the time you would want to abstract items to make things like this invisible.
    public uint Handle;

    public BufferObject(Span<TDataType> data, BufferTargetARB bufferType)
    {
        _bufferType = bufferType;

        //Getting the handle, and then uploading the data to said handle.
        Handle = Game.Gl.GenBuffer();
        BufferData(data);
    }

    public BufferObject(BufferTargetARB bufferType)
    {
        _bufferType = bufferType;

        //Getting the handle, and then uploading the data to said handle.
        Handle = Game.Gl.GenBuffer();
    }

    public void Dispose()
    {
        //Remember to delete our buffer.
        Game.Gl.DeleteBuffer(Handle);
    }

    public unsafe void BufferData(in Span<TDataType> data)
    {
        Bind();
        fixed (void* d = data)
        {
            Game.Gl.BufferData(_bufferType, (nuint) (data.Length * sizeof(TDataType)), d, BufferUsageARB.DynamicDraw);
        }
    }

    public void Bind()
    {
        //Binding the buffer object, with the correct buffer type.
        Game.Gl.BindBuffer(_bufferType, Handle);
    }
}