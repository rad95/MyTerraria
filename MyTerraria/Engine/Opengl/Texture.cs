﻿using SixLabors.ImageSharp.Drawing.Processing;

namespace MyTerraria.Engine.Opengl;

public class Texture : IDisposable
{
    public uint Handle;
    public Vector2 Size;
    public unsafe Texture(string path)
    {
        //Loading an image using imagesharp.
        var img = (Image<Rgba32>) Image.Load(path);
        Size = new(img.Height, img.Width);
        //We need to flip our image as image sharps coordinates has origin (0, 0) in the top-left corner,
        //whereas openGL has origin in the bottom-left corner.
        img.Mutate(x => x.Flip(FlipMode.Vertical));

        fixed (void* data = &MemoryMarshal.GetReference(img.GetPixelRowSpan(0)))
        {
            //Loading the actual image.
            Load(data, (uint) img.Width, (uint) img.Height);
        }

        //Deleting the img from imagesharp.
        img.Dispose();
    }

    public unsafe Texture( Image<Rgba32> img)
    {
        //We need to flip our image as image sharps coordinates has origin (0, 0) in the top-left corner,
        //whereas openGL has origin in the bottom-left corner.
        Size = new(img.Height, img.Width);
        img.Mutate(x => x.Flip(FlipMode.Vertical));

        fixed (void* data = &MemoryMarshal.GetReference(img.GetPixelRowSpan(0)))
        {
            //Loading the actual image.
            Load(data, (uint) img.Width, (uint) img.Height);
        }

        //Deleting the img from imagesharp.
        img.Dispose();
    }

    public void Dispose()
    {
        //In order to dispose we need to delete the opengl handle for the texure.
        Game.Gl.DeleteTexture(Handle);
    }

    public static Texture CreateWhitePixel()
    {
        Image<Rgba32> img = new(1, 1);
        img[0, 0] = new Rgba32(255, 255, 255);
        return new Texture(img);
    }

    private unsafe void Load(void* data, uint width, uint height)
    {
        //Generating the opengl handle;
        Handle = Game.Gl.GenTexture();
        Bind();

        //Setting the data of a texture.
        Game.Gl.TexImage2D(TextureTarget.Texture2D, 0, (int) InternalFormat.Rgba, width, height, 0, PixelFormat.Rgba,
            PixelType.UnsignedByte, data);
        //Setting some texture perameters so the texture behaves as expected.
        Game.Gl.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int) GLEnum.Repeat);
        Game.Gl.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int) GLEnum.Repeat);
        Game.Gl.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int) GLEnum.Nearest);
        Game.Gl.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int) GLEnum.Nearest);

        //Generating mipmaps.
        Game.Gl.GenerateMipmap(TextureTarget.Texture2D);
    }

    public void Bind(TextureUnit textureSlot = TextureUnit.Texture0)
    {
        //When we bind a texture we can choose which textureslot we can bind it to.
        Game.Gl.ActiveTexture(textureSlot);
        Game.Gl.BindTexture(TextureTarget.Texture2D, Handle);
    }
}