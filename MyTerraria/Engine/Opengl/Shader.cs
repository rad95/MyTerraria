﻿namespace MyTerraria.Engine.Opengl;

public class Shader : IDisposable
{
    //Our handle and the GL instance this class will use, these are private because they have no reason to be public.
    //Most of the time you would want to abstract items to make things like this invisible.
    public readonly uint Handle;

    public Shader(string vertexPath, string fragmentPath)
    {
        //Load the individual shaders.
        var vertex = LoadShader(ShaderType.VertexShader, vertexPath);
        var fragment = LoadShader(ShaderType.FragmentShader, fragmentPath);
        //Create the shader Game.
        Handle = Game.Gl.CreateProgram();
        //Attach the individual shaders.
        Game.Gl.AttachShader(Handle, vertex);
        Game.Gl.AttachShader(Handle, fragment);
        Game.Gl.LinkProgram(Handle);
        //Check for linking errors.
        Game.Gl.GetProgram(Handle, GLEnum.LinkStatus, out var status);
        if (status == 0) throw new Exception($"Program failed to link with error: {Game.Gl.GetShaderInfoLog(vertex)}");
        //Detach and delete the shaders
        Game.Gl.DetachShader(Handle, vertex);
        Game.Gl.DetachShader(Handle, fragment);
        Game.Gl.DeleteShader(vertex);
        Game.Gl.DeleteShader(fragment);
    }

    public void Dispose()
    {
        //Remember to delete the program when we are done.
        Game.Gl.DeleteProgram(Handle);
    }

    public int GetAttribLocation(string attribName)
    {
        return Game.Gl.GetAttribLocation(Handle, attribName);
    }

    public void Use()
    {
        //Using the program
        Game.Gl.UseProgram(Handle);
    }

    //Uniforms are properties that applies to the entire geometry
    public void SetUniform(string name, int value)
    {
        //Setting a uniform on a shader using a name.
        var location = Game.Gl.GetUniformLocation(Handle, name);
        if (location == -1) //If GetUniformLocation returns -1 the uniform is not found.
            throw new Exception($"{name} uniform not found on shader.");
        Game.Gl.Uniform1(location, value);
    }

    public void SetUniform(string name, float value)
    {
        var location = Game.Gl.GetUniformLocation(Handle, name);
        if (location == -1) throw new Exception($"{name} uniform not found on shader.");
        Game.Gl.Uniform1(location, value);
    }

    public void SetUniform(string name, Vector2 value)
    {
        //Setting a uniform on a shader using a name.
        var location = Game.Gl.GetUniformLocation(Handle, name);
        if (location == -1) //If GetUniformLocation returns -1 the uniform is not found.
            throw new Exception($"{name} uniform not found on shader.");
        Game.Gl.Uniform2(location, value);
    }

    public void SetUniform(string name, Vector3 value)
    {
        //Setting a uniform on a shader using a name.
        var location = Game.Gl.GetUniformLocation(Handle, name);
        if (location == -1) //If GetUniformLocation returns -1 the uniform is not found.
            throw new Exception($"{name} uniform not found on shader.");
        Game.Gl.Uniform3(location, value);
    }

    public unsafe void SetUniform(string name, Matrix4x4 value)
    {
        //A new overload has been created for setting a uniform so we can use the transform in our shader.
        var location = Game.Gl.GetUniformLocation(Handle, name);
        if (location == -1) throw new Exception($"{name} uniform not found on shader.");
        Game.Gl.UniformMatrix4(location, 1, false, (float*) &value);
    }

    private uint LoadShader(ShaderType type, string path)
    {
        //To load a single shader we need to:
        //1) Load the shader from a file.
        //2) Create the handle.
        //3) Upload the source to opengl.
        //4) Compile the shader.
        //5) Check for errors.
        var src = File.ReadAllText(path);
        var handle = Game.Gl.CreateShader(type);
        Game.Gl.ShaderSource(handle, src);
        Game.Gl.CompileShader(handle);


        return handle;
    }
}