﻿namespace MyTerraria.Engine.Opengl;

//The vertex array object abstraction.
public class VertexArrayObject<TVertexType> : IDisposable
    where TVertexType : unmanaged
{
    //Our handle and the GL instance this class will use, these are private because they have no reason to be public.
    //Most of the time you would want to abstract items to make things like this invisible.
    private readonly uint _handle;

    public VertexArrayObject(GL gl, BufferObject<TVertexType> vbo)
    {
        //Saving the GL instance.
        Game.Gl = gl;

        //Setting out handle and binding the VBO and EBO to this VAO.
        _handle = Game.Gl.GenVertexArray();
        Bind();
        vbo.Bind();
    }

    public void Dispose()
    {
        //Remember to dispose this object so the data GPU side is cleared.
        //We dont delete the VBO and EBO here, as you can have one VBO stored under multiple VAO's.
        Game.Gl.DeleteVertexArray(_handle);
    }

    public unsafe void VertexAttributePointer(uint index, int count, VertexAttribPointerType type, uint vertexSize,
        int offSet)
    {
        //Setting up a vertex attribute pointer
        Game.Gl.VertexAttribPointer(index, count, type, false, vertexSize * (uint) sizeof(TVertexType),
            (void*) (offSet * sizeof(TVertexType)));
        Game.Gl.EnableVertexAttribArray(index);
    }

    public unsafe void VertexAttributePointerWithNullOffset(uint index, int count, VertexAttribPointerType type,
        uint vertexSize)
    {
        //Setting up a vertex attribute pointer
        Game.Gl.VertexAttribPointer(index, count, type, false, vertexSize * (uint) sizeof(TVertexType), null);
        Game.Gl.EnableVertexAttribArray(index);
    }

    public void Bind()
    {
        //Binding the vertex array.
        Game.Gl.BindVertexArray(_handle);
    }
}

public class VertexArrayObject<TVertexType, TIndexType> : VertexArrayObject<TVertexType>
    where TVertexType : unmanaged
    where TIndexType : unmanaged
{
    public VertexArrayObject(GL gl, BufferObject<TVertexType> vbo, BufferObject<TIndexType> ebo) : base(gl, vbo)
    {
        ebo.Bind();
    }
}