﻿namespace MyTerraria;

public class ColorHsb
{
    private int _b;
    private int _h;
    private int _s;

    public ColorHsb(int h = 0, int s = 0, int b = 0)
    {
        _h = h;
        _s = s;
        _b = b;
    }

    public int H
    {
        get => _h;
        set
        {
            if (value > 360)
                _h = value - 360 - 1;
            else if (value < 0)
                _h = 0 + value;
            else
                _h = value;
        }
    }

    public int S
    {
        get => _s;
        set
        {
            if (value > 100)
                _s = value - 100 - 1;
            else if (value < 0)
                _s = 0 + value;
            else
                _s = value;
        }
    }

    public int B
    {
        get => _b;
        set
        {
            if (value > 100)
                _b = value - 100 - 1;
            else if (value < 0)
                _b = 0 + value;
            else
                _b = value;
        }
    }

    public override string ToString()
    {
        return $"R: {_h} G: {_s} B: {_b}";
    }
}