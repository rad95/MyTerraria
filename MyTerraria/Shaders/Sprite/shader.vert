﻿#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aUv;

out vec2 fUv;

uniform mat4 uProjectionMatrix;
uniform mat4 uCamera;

void main()
{
    gl_Position = uProjectionMatrix * uCamera * vec4(aPos, 0.0f, 1.0f);
    fUv = aUv;
}  