﻿namespace MyTerraria.MainGame;

public class TileRegistry
{
    private static readonly Dictionary<TileType, Tile> _storage = new();

    public TileRegistry Add(TileType tileType, Texture texture)
    {
        _storage[tileType] = new Tile(texture);
        return this;
    }

    public static Tile GetTile(TileType tileType)
    {
        return new Tile(_storage[tileType].Texture);
    }
}