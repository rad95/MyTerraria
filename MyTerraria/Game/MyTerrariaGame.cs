﻿using MyTerraria.Engine.Graphics.Primitives;
using MyTerraria.MainGame.Entities;
using MyTerraria.MainGame.UI;
using Rectangle = Silk.NET.Maths.Rectangle;

namespace MyTerraria.MainGame;

// TODO: сохранение миров, UI, курсор, 
public class MyTerrariaGame : Game
{
    private Label _debugInfo;
    private FpsCounter _fpsCounter;
    private Player _player;
    private World _world;
    private RectangleShape rect = new(new(100, 100), new(100, 100), new(0.5f, 0.5f, 0.5f));

    // TODO: добавить метод Add(drawable obj) который сразу будет и отрисовывать и апдейтить
    public override void OnLoad()
    {
        base.OnLoad();
        Resources.Shaders = new Dictionary<string, Shader>
        {
            ["SpriteShader"] = new(@"Shaders/Sprite/shader.vert", @"Shaders/Sprite/shader.frag"),
            ["PrimitiveShader"] = new(@"Shaders/Primitive/shader.vert", @"Shaders/Primitive/shader.frag")
        };
        Resources.Textures = new Dictionary<string, Texture>
        {
            ["whitePixel"] = Texture.CreateWhitePixel(),
            ["grass"] = new(@"Resources/Textures/grass.png"),
            ["player"] = new(@"Resources/Textures/player.png"),
            ["dirt"] = new(@"Resources/Textures/dirt.png"),
            ["2"] = new(@"Resources/Textures/2.png"),
            ["5"] = new(@"Resources/Textures/5.png"),
            ["cursor"] = new(@"Resources/Textures/cursor.png")
        };
        SpriteRenderer.Init();
        LinesRenderer.Init();
        RectangleRenderer.Init();
        UseCustomCursor = true;
        Cursor = new(Resources.Textures["cursor"]);
        _world = new World(new(50, 30));
        _player = new Player();
        _fpsCounter = new FpsCounter();
        _debugInfo = new Label("", new Vector2(0, 50), 20, Colour.White, "Arial", @"C:\Windows\Fonts/arial.ttf");
        //new TexturePack(@"Resources/Textures/Tiles_0.png", 1, 8, 8);
    }

    public override void OnUpdate(double obj)
    {
        base.OnUpdate(obj);
        _player.Update();
        SpriteRenderer.Camera.Update();
        if (Mouse.IsButtonPressed(MouseButton.Left))
        {
            var tilepos = _world.GetTilePosByScreenCoords(GetMousePos());
            for (var i = -1; i <= 1; i++)
            for (var j = -1; j <= 1; j++)
                try
                {
                    _world.Tiles[(int) tilepos.X + i, (int) tilepos.Y + j] = null;
                }
                catch (Exception e)
                {
                }
        }
        else if (Mouse.IsButtonPressed(MouseButton.Right))
        {
            var tilepos = _world.GetTilePosByScreenCoords(GetMousePos());
            for (var i = -1; i <= 1; i++)
            for (var j = -1; j <= 1; j++)
                try
                {
                    _world.SetTile((int)tilepos.X + i, (int)tilepos.Y + j, TileType.Dirt);
                }
                catch (Exception e)
                {
                }
        }

        _world.Update();
    }
    
    public override void OnRender(double delta)
    {
        var tilepos = _world.GetTilePosByScreenCoords(GetMousePos());
        if (tilepos.X.IsWithin(0, _world.WorldSize.X - 1) &&  tilepos.Y.IsWithin(0, _world.WorldSize.Y - 1))
            _world.Tiles[(int) tilepos.X, (int) tilepos.Y]?.DrawBorder(new Vector3(1f, 0f, 0f));

        base.OnRender(delta);
        _world.Draw();
        rect.Draw();
        _fpsCounter.Draw();
        _player.Draw();
        var cameraOffset = SpriteRenderer.Camera.Position - SpriteRenderer.Camera.Origin;
        _debugInfo.Draw($"campos: {(int) SpriteRenderer.Camera.Position.X} {(int) SpriteRenderer.Camera.Position.Y} ; " +
                        $"camoff: {(int) cameraOffset.X} {(int) cameraOffset.Y} mpos: {GetMousePos().X} {GetMousePos().Y}");

    }
}