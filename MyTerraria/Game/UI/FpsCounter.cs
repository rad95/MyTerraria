﻿namespace MyTerraria.MainGame.UI;

// TODO: сделать класс ui
public class FpsCounter
{
    private readonly Label _fpsLabel = new("Calculating..", new Vector2(0, 200), 20, Colour.White, "Arial",
        @"C:\Windows\Fonts/arial.ttf");

    public int MaxFps;
    public int MinFps;
    public int TempMaxFps;

    public int TempMinFps;

    // TODO: сделать класс таймера, который бы выполнял опр. действие за опр. отрезок времени какое то кол-во раз
    private double _timer;

    public FpsCounter()
    {
        _fpsLabel.ShouldDrawBackground = false;
    }

    public void Draw()
    {
        _timer += Game.Delta;
        var currentFps = (int) (1 / Game.Delta);
        if (currentFps > TempMaxFps)
            TempMaxFps = currentFps;
        else if (currentFps < TempMinFps)
            TempMinFps = currentFps;
        if (_timer >= 1)
        {
            MaxFps = TempMaxFps;
            TempMaxFps = currentFps;
            MinFps = TempMinFps;
            TempMinFps = currentFps;
            _timer = 0;
        }

        _fpsLabel.Draw($"{currentFps} FPS [Min: {MinFps}   Max: {MaxFps}]");
    }
}