﻿namespace MyTerraria.MainGame;

public class Camera
{
    private Vector2 _position;
    private float _rotation;

    private Vector3 _scale = new(1);

    // TODO: переместить window  в отдельный класс, чтобы Game.Window => Window.
    public Camera()
    {
        Game.Window.Resize += newSize => { _transform.Origin = new Vector3(newSize.X / 2, newSize.Y / 2, 0); };
        _transform.Origin = new Vector3(Game.Window.Size.X / 2, Game.Window.Size.Y / 2, 0);
    }

    public Vector2 Position
    {
        get => _position;
        set
        {
            _position = value;
            _transform.Position = new Vector3(-value, 0);
        }
    }

    public Vector2 Origin => new(_transform.Origin.X, _transform.Origin.Y);

    public float Rotation
    {
        get => _rotation;
        set
        {
            _rotation = value;
            _transform.Rotation = Quaternion.CreateFromAxisAngle(Vector3.UnitZ, value);
        }
    }

    public Vector3 Zoom
    {
        get => _scale;
        set
        {
            _scale = value;
            _transform.Scale = value;
        }
    }
    
    private Transform _transform = new();
    public Matrix4x4 ViewMatrix;


    public void Shake(float duration, float magnitude)
    {
        // TODO: 
    }

    public void Blur(bool condition)
    {
        // TODO:
    }

    public void Update()
    {
        ViewMatrix = _transform.CameraViewMatrix;
    }
}