﻿namespace MyTerraria.MainGame;

public enum TileType
{
    Dirt,
    Grass
}

public class Tile : Sprite
{
    public const int TileSize = 16;

    public Tile(Vector2 pos, Texture tex) : base(tex, new Vector2(TileSize), pos)
    {
        Origin = Vector2.Zero;
    }

    public Tile(Vector2 pos, TileType tileType) : base(new Vector2(TileSize), pos)
    {
        Origin = Vector2.Zero;
        Texture = TileRegistry.GetTile(tileType).Texture;
    }

    public Tile(Texture tex) : base(tex, new Vector2(TileSize), new Vector2(0, 0))
    {
        Origin = Vector2.Zero;
    }

    public override void Draw()
    {
        base.Draw();
    }

    public void Update()
    {
    }

    public static void RegisterTiles()
    {
        new TileRegistry().Add(TileType.Dirt, Game.Resources.Textures["dirt"])
            .Add(TileType.Grass, Game.Resources.Textures["grass"]);
    }
}