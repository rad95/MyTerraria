﻿using MyTerraria.Engine.Graphics.Primitives;
using Rectangle = SixLabors.ImageSharp.Rectangle;

namespace MyTerraria.MainGame.Entities;

public class Player : Sprite
{
    private World _world = World.GetInstance();
    public float MoveSpeed = 175f;
    public float MoveSpeedAcceleration = 5f;
    private Vector2 _velocity = Vector2.Zero;
    public int Direction = 1;
    public RectangleShape directionRect = new(new(50, 2), new(1f, 0f, 0f));

    public Vector2 SpawnPos = new(500, 300);
    private bool _isMoving;

    public Player() : base(Game.Resources.Textures["player"], new(Tile.TileSize * 1.5f, Tile.TileSize * 2.8f))
    {
        Input.AddKeyBinding(Key.Space, () => Jump());
        Input.AddKeyBinding(Key.Number1, () => Texture = Game.Resources.Textures["player"]);
        Input.AddKeyBinding(Key.Number2, () => Texture = Game.Resources.Textures["2"]);
        Origin = BottomCenter;
        Position = SpawnPos;
        directionRect.Origin = new(0, directionRect.Size.Y / 2);
    }

    public void Update()
    {
        
        HandleInput();
        UpdatePhysics();
        if (!Position.X.IsWithin(-Tile.TileSize * 3, (_world.WorldSize.X + 3) * 16) ||
            Position.Y < -Tile.TileSize * 3)
            Spawn();
        directionRect.Position = new(Position.X, Position.Y + Size.Y / 2);
        // TODO: change to setScale(directionRect.SetScaleX(Direction)
        directionRect.Scale = new(Direction, 1, 1);
    }

    private void Spawn()
    {
        Position = SpawnPos;
    }

    private void UpdatePhysics()
    {
        //Rotation += 2f * Game.Delta;
        //Scale = (float)Math.Sin(Game.DeltaSum);
        bool isFall = true;
        _velocity += new Vector2(0, 0.50f * Game.Delta);

        var plTile = _world.GetTileByWorldCoords(new(Position.X, Position.Y));
        if (plTile != null)
        {
            plTile.DrawBorder(new Vector3(0, 1, 0));
            Vector2 nextPos = Position + _velocity + Origin;
            Rect plRect = new Rect(Position, Size, Origin);
            isFall = !plRect.Intersects(plTile);
        }

        if (!isFall)
            _velocity.Y = 0;

        if (!_isMoving)
            _velocity.X = 0;
        else
            _velocity.X = Math.Clamp(_velocity.X, -MoveSpeed, MoveSpeed);

        Position -= _velocity;
        _isMoving = false;
    }

    public override void Draw()
    {
        base.Draw();
        directionRect.Draw();
    }

    private void HandleInput()
    {
        //TODO: Сделать методы для перемещения вместо свойств(MoveX, MoveY)
        //TODO: Перенести камеру в game для удобного использования
        //TODO: перенести движок в проект

        if (Game.IsKeyPressed(Key.A))
        {
            Direction = -1;
            if(Scale.X > 0)
                Scale = new Vector3(-Scale.X, 1, 1);
            _velocity.X += MoveSpeedAcceleration * Game.Delta;
            _isMoving = true;
        }

        if (Game.IsKeyPressed(Key.D))
        {
            Direction = 1;
            if(Scale.X < 0)
                Scale = new Vector3(-Scale.X, 1, 1);
            _velocity.X -= MoveSpeedAcceleration * Game.Delta;
            _isMoving = true;
        }

        if (Game.IsKeyPressed(Key.W))
            MoveY(MoveSpeed * Game.Delta);
        if (Game.IsKeyPressed(Key.S))
            MoveY(-MoveSpeed * Game.Delta);
        SpriteRenderer.Camera.Position = Position;
        
        // TODO: Перенести это в CameraController
        if (Game.IsKeyPressed(Key.Equal))
            SpriteRenderer.Camera.Zoom += new Vector3(2f * Game.Delta, 2f * Game.Delta, 1);

        if (Game.IsKeyPressed(Key.Minus))
            SpriteRenderer.Camera.Zoom -= new Vector3(2f * Game.Delta, 2f * Game.Delta, 1);
    }

    public void Jump()
    {
        _velocity.Y = -0.25f;
    }
}