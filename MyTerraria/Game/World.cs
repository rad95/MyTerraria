﻿namespace MyTerraria.MainGame;

public class World
{
    private static World _instance;
    public readonly Vector2D<int> WorldSize;
    public Tile[,] Tiles;

    public World(Vector2D<int> worldSize)
    {
        WorldSize = worldSize;
        Tiles = new Tile[worldSize.X, worldSize.Y];
        _instance = this;
        Tile.RegisterTiles();
        GenerateWorld();
    }

    // TODO: Refactor singleton(use nuget for example)
    public static World GetInstance()
    {
        return _instance;
    }
    
    public void GenerateWorld()
    {
        // травяной пол
        for (var x = 1; x <= WorldSize.X; x++) 
            SetTile(x, WorldSize.Y / 2 - 1, TileType.Grass);
        // грязь
        for (var x = 1; x <= WorldSize.X; x++)
            for (var y = 1; y < WorldSize.Y / 2 - 1; y++)
                SetTile(x, y, TileType.Dirt);
        // стены
        for (var y = WorldSize.Y / 2 - 1; y <= WorldSize.Y; y++)
        {
            SetTile(1, y, TileType.Dirt);
            SetTile(2, y, TileType.Dirt);
            SetTile(WorldSize.X, y, TileType.Dirt);
            SetTile(WorldSize.X - 1, y, TileType.Dirt);
        }

    }
    public void GeneratePlainWorld()
    {
        for (int x = 1; x <= WorldSize.X; x++)
        {
            for (int y = 1; y <= WorldSize.Y; y++)
            {
                SetTile(x, y, TileType.Dirt);
            }
        }
    }

    /// <summary>
    ///     Places tile at <paramref name="position" />
    /// </summary>
    /// <remarks>grid included(&lt;2, 3&gt; => &lt;16,24&gt;)</remarks>
    /// <param name="position">Position of tile</param>
    /// <param name="tileType">Type of tile</param>
    public void SetTile(Vector2D<int> position, TileType tileType)
    {
        if (position.X < 1 || position.Y < 1 || position.X > WorldSize.X || position.Y > WorldSize.Y)
            return;
        Tiles[position.X - 1, position.Y - 1]
            = new Tile(new Vector2((position.X - 1) * Tile.TileSize, (position.Y - 1) * Tile.TileSize), tileType);
    }

    /// <summary>
    ///     Places tile at &lt;<paramref name="x" /> <paramref name="y" />&gt;
    /// </summary>
    /// <remarks>grid included(&lt;2, 3&gt; => &lt;16,24&gt;)</remarks>
    /// <param name="x">X coordinate of tile</param>
    /// <param name="y">Y coordinate of tile</param>
    /// <param name="tileType">Type of tile</param>
    public void SetTile(int x, int y, TileType tileType)
    {
        if (x < 1 || y < 1 || x > WorldSize.X || y > WorldSize.Y)
            return;
        Tiles[x - 1, y - 1] = new Tile(new Vector2((x - 1) * Tile.TileSize, (y - 1) * Tile.TileSize), tileType);
    }

    /// <summary>
    ///     Gets tile index in <c>Tiles[]</c>
    /// </summary>
    /// <param name="screenCoords">Screen point coordinates</param>
    /// <returns>Tile index in <c>Tiles[]</c></returns>
    public Vector2 GetTilePosByScreenCoords(Vector2 screenCoords)
    {
        // TODO: учитывать зум камеры
        var res = (screenCoords + SpriteRenderer.Camera.Position - SpriteRenderer.Camera.Origin) 
                  / Tile.TileSize;
        res.X = (int) res.X;
        res.Y = (int) res.Y;
        return res;
    }

    public Vector2 GetTilePosByWorldCoords(Vector2 worldCoords) 
        => worldCoords / Tile.TileSize;

    public Tile GetTileByWorldCoords(Vector2 worldCoords)
    {
        var tilePos = worldCoords / Tile.TileSize;
        if (tilePos.X.IsWithin(0, WorldSize.X) && tilePos.Y.IsWithin(0, WorldSize.Y) &&
            Tiles[(int) tilePos.X, (int) tilePos.Y] != null)
            return Tiles[(int) tilePos.X, (int) tilePos.Y];
        return null;
    }
        


    public void Draw()
    {

        // TODO: создать отсортированный список тайлов по тайлтайпу и рисовать по этому списку
        for (int x = 0; x < WorldSize.X; x++)
        {
            for (int y = 0; y < WorldSize.Y; y++)
            {
                Tiles[x, y]?.Draw();
            }
        }


    }
    public void Update()
    {
        foreach (var tile in Tiles)
            tile?.Update();
    }
}