﻿namespace MyTerraria;

public class ColorRgb
{
    private int _b;
    private int _g;
    private int _r;

    public ColorRgb(int r = 0, int g = 0, int b = 0)
    {
        _r = r;
        _g = g;
        _b = b;
    }

    public int R
    {
        get => _r;
        set
        {
            if (value > 255)
                _r = value - 255 - 1;
            else if (value < 0)
                _r = 0 + value;
            else
                _r = value;
        }
    }

    public int G
    {
        get => _g;
        set
        {
            if (value > 255)
                _g = value - 255 - 1;
            else if (value < 0)
                _g = 0 + value;
            else
                _g = value;
        }
    }

    public int B
    {
        get => _b;
        set
        {
            if (value > 255)
                _b = value - 255 - 1;
            else if (value < 0)
                _b = 0 + value;
            else
                _b = value;
        }
    }

    public override string ToString()
    {
        return $"R: {_r} G: {_g} B: {_b}";
    }

    public Vector3 ToNormalized()
    {
        return new Vector3(_r / (float) 255, _g / (float) 255, _b / (float) 255);
    }
}